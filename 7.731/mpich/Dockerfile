# Build stage with Spack pre-installed and ready to be used
FROM alexpacheco/spack-centos-gcc:7.731

# What we want to install and how we want to install it
# is specified in a manifest file (spack.yaml)
RUN mkdir /opt/spack-environment \
&&  (echo "spack:" \
&&   echo "  specs:" \
&&   echo "  - mpich" \
&&   echo "  concretization: together" \
&&   echo "  config:" \
&&   echo "    install_tree: /opt/software" \
&&   echo "  view: /opt/view") > /opt/spack-environment/spack.yaml

# Install the software, remove unecessary deps
RUN cd /opt/spack-environment && \
    spack env activate -d . && spack install && spack gc -y

# Strip all the binaries
RUN find -L /opt/view/* -type f -exec readlink -f '{}' \; | \
    xargs file -i | \
    grep 'charset=binary' | \
    grep 'x-executable\|x-archive\|x-sharedlib' | \
    awk -F: '{print $1}' | xargs strip -s

# Modifications to the environment that are necessary to run
RUN cd /opt/spack-environment && \
    spack env deactivate && \
    spack env activate --sh -d . >> /etc/profile.d/z10_spack_environment.sh

RUN echo 'export PS1="\[$(tput bold)\]\[$(tput setaf 1)\][mpich-gcc-731]\[$(tput setaf 2)\]\u\[$(tput sgr0)\]:\w $ \[$(tput sgr0)\]"' >> ~/.bashrc

LABEL "mpi"="mpich"

ENV PATH=/opt/view/bin:$PATH
ENV LD_LIBRARY_PATH=/opt/view/lib64:/opt/view/lib:$LD_LIBRARY_PATH
ENV LD_INCLUDE_PATH=/opt/view/include:$LD_INCLUDE_PATH
ENV MANPATH=/opt/share/man:$MANPATH

ENTRYPOINT ["/bin/bash", "--rcfile", "/etc/profile", "-l"]
