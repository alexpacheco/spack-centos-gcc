# Build stage with Spack pre-installed and ready to be used
FROM spack/centos7:latest

# Configure Spack
RUN (echo "config:" \
&&   echo "  install_path_scheme: '\${ARCHITECTURE}/\${COMPILERNAME}-\${COMPILERVER}/\${PACKAGE}/\${VERSION}-\${HASH:7}'" \
&&   echo "  build_jobs: 1") > /opt/spack/etc/spack/config.yaml

RUN (echo "modules:" \
&&   echo "  enable:" \
&&   echo "    - tcl" \
&&   echo "  tcl:" \
&&   echo "    hash_length: 0" \
&&   echo "    naming_scheme: '\${PACKAGE}/\${VERSION}/\${COMPILERNAME}-\${COMPILERVER}'" \
&&   echo "    blacklist_implicits: true") > /opt/spack/etc/spack/modules.yaml 

RUN (echo "packages:" \
&&   echo "   all:" \
&&   echo "     target: [x86_64]" ) > /opt/spack/etc/spack/packages.yaml

# Install gcc 8.x from devtools
RUN yum update -y \
 && yum install -y centos-release-scl \
 && yum -y --enablerepo=centos-sclo-rh-testing install devtoolset-8-gcc devtoolset-8-gcc-c++ devtoolset-8-gcc-gfortran \
 && rm -rf /var/cache/yum \
 && yum clean all 

# Add gcc 8.x to compiler list
RUN . /opt/rh/devtoolset-8/enable \
 && . /opt/spack/share/spack/setup-env.sh\
 && spack compiler add \
 && mv /root/.spack/linux/compilers.yaml /opt/spack/etc/spack/compilers.yaml

# Setup paths for gcc 8.x and spack
RUN ln -s /opt/rh/devtoolset-8/enable /etc/profile.d/z20_devtools8.sh
RUN echo source /opt/spack/share/spack/setup-env.sh > /etc/profile.d/z05_spack.sh

ENTRYPOINT ["/bin/bash", "--rcfile", "/etc/profile", "-l"]

